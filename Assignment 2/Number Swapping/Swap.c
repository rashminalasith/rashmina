#include <stdio.h>

int main(){
    int a,b;
    printf("Give the first integer(a): ");
    scanf("%d", &a);
    printf("Give the second integer(b): ");
    scanf("%d",&b);
    printf("The integers before swapping: a = %d , b = %d \n", a,b);
    a = a + b;
    b = a - b;
    a = a - b;
    printf("The integers after swapping: a = %d , b = %d \n", a,b);
    
    return 0;
}