#include <stdio.h>


int main(){
    float a,b,c;
    printf("Give the first floating point number:  ");
    scanf("%f", &a);
    printf("Give the second floating point number:  ");
    scanf("%f", &b);
    c = a * b;
    printf("Product of the two numbers = %.2f \n ", c);
    
    return 0;

}
