#include<stdio.h>

void Rows(int x);
void pattern(int n);
int NoofRows=1;

void Rows(int x){
    if(x>0){
        printf("%d",x);
        Rows(x-1);
    }
}

void pattern(int n){
    if (n>0){
        Rows(NoofRows);
        printf("\n");
        NoofRows++;
        pattern(n-1);
    }
}

int main(){
    int rows;
    printf("Enter the number of rows: ");
    scanf("%d",&rows);
    pattern(rows);
    return 0;
}