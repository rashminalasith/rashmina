#include<stdio.h>
#define SIZE 100

void main()
{
	int a[SIZE][SIZE], b[SIZE][SIZE], add[SIZE][SIZE] = {0}, mul[SIZE][SIZE] = {0};
	int i,j,k,m,n,p,q;
	printf("Enter no. of rows and columns in matrix 1: ");
	scanf("%d%d",&m,&n);
	printf("Enter no. of rows and columns in matrix 2: ");
	scanf("%d%d",&p,&q);
	
		printf("Enter elements of matrix A: \n");
		for(i=0;i<m;i++)
			for(j=0;j<n;j++)
				scanf("%d", &a[i][j]);
		printf("Enter elements of matrix B: \n");
		for(i=0;i<p;i++)
			for(j=0;j<q;j++)
				scanf("%d", &b[i][j]);
	//Addition			
	if ( m == p && n == q){
        for(i=0;i<m;i++)
                for(j=0;j<n;j++)
                    add[i][j] = a[i][j] + b[i][j];
            printf("\nResult of Matirx Addition:\n");
            for(i=0;i<m;i++)
            {
                for(j=0;j<n;j++)
                    printf("%d ", add[i][j]);
                printf("\n");
            }
	}
	//Multiplication
	if(n == p){
		
		for(i=0;i<m;i++)
			for(j=0;j<q;j++)
				for(k=0;k<p;k++)
					mul[i][j] += a[i][k]*b[k][j];
		printf("\nResult of Matirx Multiplication:\n");
		for(i=0;i<m;i++)
		{
			for(j=0;j<q;j++)
				printf("%d ", mul[i][j]);
			printf("\n");
		}
	}
	return 0;
	
}

    
